import React from 'react';
import Button from './components/button';
import Modal from './components/modal';
import './App.css';

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      modalFirst: {
        isOpen: false,
        id: "modalFirst",
        title: "titleDelete",
        text: "Do you want delete?",
        isCloseBtn: true,
        onClose: (id) => this.closeModal(id),
        actions: {
          btnSubmit: <Button text="Submit" styles="yellow" onClick={() => { this.closeModal("modalFirst") }} />,
          btnCancel: <Button text="Cancel" styles="purple" onClick={() => { this.closeModal("modalFirst") }} />
        }
      },
      modalSecond: {
        isOpen: false,
        title: "create",
        id: "modalSecond",
        text: "Do you want create?",
        isCloseBtn: false,
        onClose: (id) => this.closeModal(id),
        actions: {
          btnSubmit: <Button text="Submit" styles="grey" onClick={() => { this.closeModal("modalFirst") }} />,
          btnCancel: <Button text="Cancel" styles="blue" onClick={() => { this.closeModal("modalFirst") }} />
        }
      },
    }
  }
  openModal(id) {
    this.setState(current => {
      const newState = { ...current }
      newState[id].isOpen = true
      return newState;
    })
  }
  closeModal(id) {
    console.log(id);
    this.setState(current => {
      const newState = { ...current }
      newState[id].isOpen = false

      return newState;
    })
  }
  render() {

    return (
      <div className="App">
        <Button text="open first modal" styles="red" onClick={() => { this.openModal("modalFirst") }} />
        <Button text="open second modal" styles="green" onClick={() => { this.openModal("modalSecond") }} />
        {this.state.modalFirst.isOpen && <Modal {...this.state.modalFirst} />}
        {this.state.modalSecond.isOpen && <Modal {...this.state.modalSecond} />}

      </div>
    );
  }
}
export default App;
