import React from "react";
import {StyledModal, ModalContent, StyledClose} from "./styled"
class Modal extends React.Component {
    render() {
        const {title, onClose, id, text, isCloseBtn, actions } = this.props
        console.log(actions);
        return (
            <StyledModal onClick = {()=>{onClose(id)}} >
                <ModalContent onClick={e => e.stopPropagation()} >
                    {isCloseBtn && <StyledClose  onClick = {()=>{onClose(id)}} >&times;</StyledClose>}
                    <h2>{title}</h2>
                    <p>{text}</p>
                    {actions?.btnSubmit && actions.btnSubmit}
                    {actions?.btnCancel && actions.btnCancel}
                </ModalContent>
            </StyledModal>
        )
    }
}
export default Modal