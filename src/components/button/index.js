import React from "react"
import {StyledBtn} from "./styled"
class Button extends React.Component {
    render() {
        const {
            text, onClick, styles
        } = this.props
        return (
            <StyledBtn onClick={onClick} $bgStyle = {styles} >{text}</StyledBtn>
        )
    }
}
export default Button